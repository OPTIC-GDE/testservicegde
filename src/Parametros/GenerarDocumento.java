/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author joans
 */
public class GenerarDocumento extends Usuario {

    @SerializedName("acronimo")
    private String acronimo;

    @SerializedName("destinatarios")
    private String[] destinatarios;

    @SerializedName("destinatariosCopia")
    private String[] destinatariosCopia;

    @SerializedName("destinatariosCopiaOculta")
    private String[] destinatariosCopiaOculta;

    @SerializedName("mensaje")
    private String mensaje;

    @SerializedName("referencia")
    private String referencia;

    @SerializedName("sistemaOrigen")
    private String sistemaOrigen;

    @SerializedName("tipoArchivo")
    private String tipoArchivo;

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public String[] getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(String[] destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String[] getDestinatarioCopia() {
        return destinatariosCopia;
    }

    public void setDestinatarioCopia(String[] destinatarioCopia) {
        this.destinatariosCopia = destinatarioCopia;
    }

    public String[] getDestinatarioCopiaOculta() {
        return destinatariosCopiaOculta;
    }

    public void setDestinatarioCopiaOculta(String[] destinatarioCopiaOculta) {
        this.destinatariosCopiaOculta = destinatarioCopiaOculta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(String sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

 

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

}
