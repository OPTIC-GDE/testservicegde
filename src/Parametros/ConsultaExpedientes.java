/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;


/**
 *
 * @author joans
 */
public class ConsultaExpedientes extends Usuario {

    private String trata;

    private String fechaDesde;

    private String fechaHasta;

    private int numero;

    private String creador;

    private String reparticion;

    public String getTrata() {
        return trata;
    }

    public void setTrata(String trata) {
        this.trata = trata;
    }

    public String getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(String fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public String getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(String fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCreador() {
        return creador;
    }

    public void setCreador(String creador) {
        this.creador = creador;
    }

    public String getReparticion() {
        return reparticion;
    }

    public void setReparticion(String reparticion) {
        this.reparticion = reparticion;
    }

    public String toParametros() {
        String res = "";
        String separador = "?";
        if (this.trata != null) {
            res += separador + "trata=" + this.trata;
            separador = "&";
        }
        if (this.fechaDesde != null) {
            res += separador + "fechaDesde=" + this.fechaDesde;
            separador = "&";
        }
        if (this.fechaHasta != null) {
            res += separador+"fechaHasta=" + this.fechaHasta;
            separador = "&";

        }
        if (this.numero != 0) {
            res += separador+"numero=" + this.numero;
            separador = "&";
        }
        if (this.creador != null) {
            res += separador+"numero=" + this.numero;
            separador = "&";

        }
        if (this.reparticion != null) {
            res += separador+"reparticion=" + this.reparticion;
            separador = "&";

        }

        return res;
    }
}
