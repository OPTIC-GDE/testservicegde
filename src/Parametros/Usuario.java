/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author joans
 */
public class Usuario {
    
    @SerializedName("usuario")
    protected String usuario;
    
    
    @SerializedName("ambiente")
    protected String ambiente;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }
    
}
