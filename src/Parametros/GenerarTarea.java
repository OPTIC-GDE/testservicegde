/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author joans
 */
    public class GenerarTarea extends Usuario {

        @SerializedName("acronimo")
        private String acronimo;

        @SerializedName("enviarCorreoReceptor")
        private boolean enviarCorreoReceptor;

        @SerializedName("destinatarios")
        private String[] destinatarios;

        @SerializedName("destinatariosConCopia")
        private String[] destinatariosConCopia;

        @SerializedName("destinatariosConCopiaOculta")
        private String[] destinatariosConCopiaOculta;

        @SerializedName("recibirAvisoDeFirma")
        private boolean recibirAvisoDeFirma;

        @SerializedName("referencia")
        private String referencia;

        @SerializedName("sistemaIniciador")
        private String sistemaIniciador;

        @SerializedName("tarea")
        private String tarea;

        @SerializedName("tipoArchivo")
        private String tipoArchivo;

        @SerializedName("firmantes")
        private String[] firmantes;

        @SerializedName("usuarioReceptor")
        private String usuarioReceptor;

        @SerializedName("mensaje")
        private String mensaje;

        @SerializedName("mensajeADestinatarios")
        private String mensajeADestinatario;

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public boolean isEnviarCorreoReceptor() {
        return enviarCorreoReceptor;
    }

    public void setEnviarCorreoReceptor(boolean enviarCorreoReceptor) {
        this.enviarCorreoReceptor = enviarCorreoReceptor;
    }

    public String[] getDestinatarios() {
        return destinatarios;
    }

    public void setDestinatarios(String[] destinatarios) {
        this.destinatarios = destinatarios;
    }

    public String[] getDestinatariosConCopia() {
        return destinatariosConCopia;
    }

    public void setDestinatariosConCopia(String[] destinatariosConCopia) {
        this.destinatariosConCopia = destinatariosConCopia;
    }

    public String[] getDestinatariosConCopiaOculta() {
        return destinatariosConCopiaOculta;
    }

    public void setDestinatariosConCopiaOculta(String[] destinatariosConCopiaOculta) {
        this.destinatariosConCopiaOculta = destinatariosConCopiaOculta;
    }

    public boolean isRecibirAvisoDeFirma() {
        return recibirAvisoDeFirma;
    }

    public void setRecibirAvisoDeFirma(boolean recibirAvisoDeFirma) {
        this.recibirAvisoDeFirma = recibirAvisoDeFirma;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getSistemaIniciador() {
        return sistemaIniciador;
    }

    public void setSistemaIniciador(String sistemaIniciador) {
        this.sistemaIniciador = sistemaIniciador;
    }

    public String getTarea() {
        return tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String[] getFirmantes() {
        return firmantes;
    }

    public void setFirmantes(String[] firmantes) {
        this.firmantes = firmantes;
    }

    public String getUsuarioReceptor() {
        return usuarioReceptor;
    }

    public void setUsuarioReceptor(String usuarioReceptor) {
        this.usuarioReceptor = usuarioReceptor;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMeensajeADestinatario() {
        return mensajeADestinatario;
    }

    public void setMeensajeADestinatario(String meensajeADestinatario) {
        this.mensajeADestinatario = meensajeADestinatario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }



}
