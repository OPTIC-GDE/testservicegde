/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;

import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author joans
 */
public class GenerarPase extends Usuario {

    @SerializedName("expediente")
    private String expediente;

    @SerializedName("vaMesa")
    private boolean vaMesa;

    @SerializedName("vaReparticion")
    private boolean vaReparticion;

    @SerializedName("nuevoEstado")
    private String nuevoEstado;

    @SerializedName("motivo")
    private String motivo;

    @SerializedName("reparticionDestino")
    private String reparticionDestino;

    @SerializedName("sectorDestino")
    private String sectorDestino;

    @SerializedName("sistemaOrigen")
    private String sistemaOrigen;

    @SerializedName("usuarioDestino")
    private String usuarioDestino;

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public boolean isVaMesa() {
        return vaMesa;
    }

    public void setVaMesa(boolean vaMesa) {
        this.vaMesa = vaMesa;
    }

    public boolean isVaReparticion() {
        return vaReparticion;
    }

    public void setVaReparticion(boolean vaReparticion) {
        this.vaReparticion = vaReparticion;
    }

    public String getNuevoEstado() {
        return nuevoEstado;
    }

    public void setNuevoEstado(String nuevoEstado) {
        this.nuevoEstado = nuevoEstado;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getReparticionDestino() {
        return reparticionDestino;
    }

    public void setReparticionDestino(String reparticionDestino) {
        this.reparticionDestino = reparticionDestino;
    }

    public String getSectorDestino() {
        return sectorDestino;
    }

    public void setSectorDestino(String sectorDestino) {
        this.sectorDestino = sectorDestino;
    }

    public String getSistemaOrigen() {
        return sistemaOrigen;
    }

    public void setSistemaOrigen(String sistemaOrigen) {
        this.sistemaOrigen = sistemaOrigen;
    }

    public String getUsuarioDestino() {
        return usuarioDestino;
    }

    public void setUsuarioDestino(String usuarioDestino) {
        this.usuarioDestino = usuarioDestino;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

 

    public String getAmbiente() {
        return ambiente;
    }

    @Override
    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }


}
