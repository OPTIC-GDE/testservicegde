/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author joans
 */
public class AsociarExpediente extends Usuario{
    
    @SerializedName("sistema")
    private String sistema;
    
    @SerializedName("expedientes")
    private String[] expedientes;

    @SerializedName("desasociar")
    private boolean desasociar;

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String[] getExpedientes() {
        return expedientes;
    }

    public void setExpedientes(String[] expedientes) {
        this.expedientes = expedientes;
    }

    public boolean isDesasociar() {
        return desasociar;
    }

    public void setDesasociar(boolean desasociar) {
        this.desasociar = desasociar;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }
    

}
