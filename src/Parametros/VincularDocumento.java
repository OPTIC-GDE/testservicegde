/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Parametros;
import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author joans
 */
public class VincularDocumento extends Usuario {

    @SerializedName("sistema")
    protected String sistema;

    @SerializedName("expediente")
    protected String expediente;

    @SerializedName("documentos")
    protected String[] documentos;

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getExpediente() {
        return expediente;
    }

    public void setExpediente(String expediente) {
        this.expediente = expediente;
    }

    public String[] getDocumentos() {
        return documentos;
    }

    public void setDocumentos(String[] documentos) {
        this.documentos = documentos;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }


    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

  

}
