/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaswsgde;

import Parametros.AsociarExpediente;
import Parametros.ConsultaDocumento;
import Parametros.ConsultaExpedientes;
import Parametros.DescargarDocumento;
import Parametros.GenerarDocumento;
import Parametros.GenerarExpediente;
import Parametros.GenerarPase;
import Parametros.GenerarTarea;
import Parametros.Mensaje;
import Parametros.Servicios;
import Parametros.VincularDocumento;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.thinknet.integrabilidad.autenticacion.IAutenticacionProxy;
import com.thinknet.integrabilidad.autorizacion.IAutorizacionProxy;
import com.thinknet.integrabilidad.autorizacion.TResultadoServicio3;
import encriptacion.RSAUtil;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.Base64;
import java.util.Hashtable;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 *
 * @author joans
 */
public class PruebasWSGDE {

    public class Expediente {

        @SerializedName("nroExpediente")
        String nroExpediente;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws RemoteException, Exception {
        /*  Gson gson = new Gson();
       // System.out.println("Se creo expediente");
        //Expediente exp = gson.fromJson(generarExpediente(), Expediente.class);
        //System.out.println(exp.nroExpediente);
        System.out.println("se genera documento");
        generarDocumento();
        System.out.println("se genera tarea");
        generarTareaFirmar();
      System.out.println("Se ejecuto vincular");
       //vincularDocumento(exp.nroExpediente,"IF-2020-00000181-NEU-OPT#SGP");
       
        System.out.println("Se ejecuto pase ");
       //realizarPase(exp.nroExpediente);
        System.out.println("Se consulta doc");
         //consultarDocumento("ACTA-2020-00000237-NEU-OPT#SGP");
         
           //       consultarDocumento("ACTA-2020-00000237-NEU-OPT#SGP");
         */
        //System.out.println(RSAUtil.encrypt("Neuquen456"));
        //generarTareaFirmar();
//
        //generarExpediente();
        //  String[] expedientes = {"EX-2020-00000458- -NEU-OPT#SGP","EX-2020-00000453- -NEU-OPT#SGP"};
        // asociarExpedientes(expedientes);
        consultarDocumento("IF-2020-00000331-NEU-OPT#SGP");
        //descargarDocumento("IF-2020-00000331-NEU-OPT#SGP");
        //  descargarDocumento("DECRE-2018-00027945-NEU-GPN")

       // generarExpediente();
        //consultarExpedientes();

    }

    private static String testService(String parametros, String servicio) throws Exception {

        return testService(parametros, servicio, null);
    }

    private static String testService(String parametros, String servicio, byte[] cuerpo) throws Exception {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        Mensaje msg = new Mensaje();
        msg.setParametros(parametros);
        //String encriptedKey = RSAUtil.encrypt("Noleomas94");
        String encriptedKey = RSAUtil.encrypt("123Joan");

        // encriptedKey = "hPEteQfHpqovKphOZjXPAbolbhIV7hDR8vuYowDN6c2KNPkmzSf4gS4atCHO/SCHvaCeUW8Xz4zEKIylOsWni9MmEYq9ZuHkjbXvhgbHx9boR3+TlWNKqqzPhfYVEjpPgyfNPyoVAYKh86xlA/oxqNEnzk5ukKjogHDPr4hQ6bI=";
        msg.setClave(encriptedKey);
        if (cuerpo != null) {
            msg.setArchivo(cuerpo);
        }
        String parametro = gson.toJson(msg);
        IAutenticacionProxy proxy = new IAutenticacionProxy();
        proxy.setEndpoint("http://autentica.neuquen.gov.ar:8080/scripts/autenticacion.exe/soap/IAutenticacion");

        String session = proxy.loginPecas("sistema_GDE", "Ksdf**192");
        // String session = proxy.loginPecas("Usuario-test", "asml8372");
        IAutorizacionProxy proxyAutorizacion = new IAutorizacionProxy();
        proxyAutorizacion.setEndpoint("http://autoriza.neuquen.gov.ar:8080/scripts/autorizacion.exe/soap/IAutorizacion");

        byte[] a = gson.toJson(msg).getBytes();
        byte[] cuerpo2 = "hola".getBytes();
        String mg = new String(a);

        System.out.println("Parametros:\n" + mg);

        TResultadoServicio3 res = proxyAutorizacion.solicitar_Servicio3(session, "testing", "GP-GDE", servicio, "dni=38298678", null, a, null, false, false);
        System.out.println(res.getCodResultado());
        System.out.println(res.getMensajeResultado());
        System.out.println(new String(res.getResultado1()));

        return new String(res.getResultado1());
    }

    public static String consultarExpedientes() throws Exception {
        Gson gson = new Gson();
        ConsultaExpedientes aso = new ConsultaExpedientes();
        aso.setAmbiente("test");
        aso.setReparticion("DGDE#SGP");
        aso.setUsuario("JSTACCO");
        aso.setFechaDesde("01/01/2020");        
        aso.setFechaHasta("04/01/2020");
        aso.setTrata("GSUMINISTRO");
        testService(gson.toJson(aso), Servicios.consultarExpedientes);

        return "ok";
    }

    public static String asociarExpedientes(String[] expedientes) throws Exception {
        Gson gson = new Gson();
        AsociarExpediente aso = new AsociarExpediente();
        aso.setAmbiente("test");
        aso.setUsuario("JSTACCO");
        aso.setDesasociar(false);
        aso.setSistema("test");
        aso.setExpedientes(expedientes);
        testService(gson.toJson(aso), Servicios.asociarExpedientes);

        return "ok";
    }

    public static String consultarDocumento(String nroDocumento) throws Exception {
        Gson gson = new Gson();

        ConsultaDocumento parametroAux = new ConsultaDocumento();
        parametroAux.setAmbiente("test");
        parametroAux.setUsuario("JSTACCO");
        parametroAux.setSistema("EE");
        parametroAux.setNumeroSade(nroDocumento);
//        parametroAux.setNumeroProceso("procesoGEDO.77747228");

        testService(gson.toJson(parametroAux), Servicios.consultaGedo);

        return "ok";
    }

    public static String descargarDocumento(String nroDocumento) throws Exception {
        Gson gson = new Gson();

        DescargarDocumento parametroAux = new DescargarDocumento();
        parametroAux.setAmbiente("test");
        parametroAux.setUsuario("JSTACCO");
        parametroAux.setNumeroSade(nroDocumento);

        testService(gson.toJson(parametroAux), Servicios.descargarGedo);

        return "ok";
    }

    public static String bloqueo() throws Exception {
        Gson gson = new Gson();
        // String[] docs = {"IF-2019-00036836-NEU-OPT#SGP"};
        ConsultaDocumento parametroAux = new ConsultaDocumento();
        parametroAux.setAmbiente("test");

        parametroAux.setUsuario("JSTACCO");
        parametroAux.setSistema("RH");
        //  parametroAux.setNumeroProceso("procesoGEDO.67766263");
        //parametroAux.setNumeroSade("IF-2019-00036836-NEU-OPT#SGP");
        testService(gson.toJson(parametroAux), Servicios.bloqueoDeExpedientes);

        return "ok";
    }

    public static String generarExpediente() throws Exception {
        Gson gson = new Gson();
        GenerarExpediente parametroAux = new GenerarExpediente();

        parametroAux.setSistema("RH");
        parametroAux.setAmbiente("test");

        parametroAux.setUsuario("JSTACCO");
        parametroAux.setTrata("GENE00000");
        parametroAux.setDescripcion("Una descricion para el expediente bien piola");
        parametroAux.setMotivo("Un motivo de prueba");
        parametroAux.setEmail("joan.stacco@gmail.com");
        parametroAux.setTelefono("32132311231");
        String mensajeAEncriptar = gson.toJson(parametroAux);
        return testService(mensajeAEncriptar, Servicios.generarExpediente);
    }

    public static String generarTareaConfeccionar() throws Exception {
        Gson gson = new Gson();
        GenerarTarea parametroAux = new GenerarTarea();
        String[] destinatarios = {"JSTACCO"};
        parametroAux.setAmbiente("test");

        parametroAux.setUsuario("JSTACCO");
        parametroAux.setAcronimo("IF");
        parametroAux.setUsuarioReceptor("MKLETZENBAUER");
        parametroAux.setDestinatarios(destinatarios);
        //   parametroAux.setDestinatariosConCopia(destinatarios);
        // parametroAux.setDestinatariosConCopiaOculta(destinatarios);
        parametroAux.setEnviarCorreoReceptor(true);
        parametroAux.setMeensajeADestinatario("Un mensaje de prueba");
        parametroAux.setMensaje("un mensaje de prubea");
        parametroAux.setRecibirAvisoDeFirma(true);
        parametroAux.setSistemaIniciador("test");
        parametroAux.setTarea("confeccionar documento");

        String mensajeAEncriptar = gson.toJson(parametroAux);
        return testService(mensajeAEncriptar, Servicios.generarTarea);

    }

    public static String generarTareaFirmar() throws Exception {
        Gson gson = new Gson();
        GenerarTarea parametroAux = new GenerarTarea();
        String[] firmantes = {"JSTACCO"};
        String[] destinatariosCopia = {""};
        parametroAux.setAmbiente("prod");

        parametroAux.setUsuario("JSTACCO");
        parametroAux.setAcronimo("IF");
        // parametroAux.setDestinatariosConCopia(destinatariosCopia);
        // parametroAux.setDestinatariosConCopiaOculta(destinatarios);
        parametroAux.setEnviarCorreoReceptor(true);
        parametroAux.setMeensajeADestinatario("Un mensaje de prueba");
        parametroAux.setMensaje("un mensaje de prubea");
        parametroAux.setRecibirAvisoDeFirma(true);
        parametroAux.setSistemaIniciador("EE");
        parametroAux.setFirmantes(firmantes);
        parametroAux.setTipoArchivo("txt");
        parametroAux.setTarea("firmar documento");
        parametroAux.setReferencia("una referencia para firmar");

        String mensajeAEncriptar = gson.toJson(parametroAux);

        testService(mensajeAEncriptar, Servicios.generarTarea, leerArchivo("C:\\Users\\joans\\Desktop\\trabajo\\archivo.js"));

        return gson.toJson(parametroAux);
    }

    public static String vincularDocumento(String numeroExp, String documento) throws Exception {
        Gson gson = new Gson();
        VincularDocumento vinc = new VincularDocumento();
        String[] docs = {"IF-2020-00000181-NEU-OPT#SGP"};
        vinc.setSistema("test");
        vinc.setAmbiente("test");

        vinc.setUsuario("JSTACCO");

        vinc.setExpediente(numeroExp);
        vinc.setDocumentos(docs);
        String mensajeAEncriptar = gson.toJson(vinc);

        testService(mensajeAEncriptar, Servicios.vincularDocumento);
        return "";
    }

    public static String realizarPase(String numeroExp) throws Exception {
        Gson gson = new Gson();
        GenerarPase parametroAux = new GenerarPase();

        parametroAux.setAmbiente("test");

        parametroAux.setUsuario("JSTACCO");
        parametroAux.setMotivo("un motivo de prueba");
        parametroAux.setNuevoEstado("Tramitacion");
        parametroAux.setUsuarioDestino("MAMARTINEZ");
        parametroAux.setExpediente(numeroExp);
        parametroAux.setSistemaOrigen("test");

        String mensajeAEncriptar = gson.toJson(parametroAux);
        testService(mensajeAEncriptar, Servicios.generarPase);

        return gson.toJson(parametroAux);
    }

    public static String generarDocumento() throws Exception {
        Gson gson = new Gson();
        GenerarDocumento parametroAux = new GenerarDocumento();
        parametroAux.setAmbiente("cap");

        parametroAux.setUsuario("JSTACCO");
        parametroAux.setAcronimo("NOTI");
        parametroAux.setReferencia("UNA REFERENCIA DE PRUEBA");
        //      //  String[] destinatarios = {"MAMARTINEZ", "JSTACCO"};
        String[] destinatarios = {"JSTACCO"};
        parametroAux.setDestinatarios(destinatarios);
        parametroAux.setTipoArchivo("pdf");
        // parametroAux.setMensaje("un mensaje de prueba");
        parametroAux.setSistemaOrigen("test");

        String mensajeAEncriptar = gson.toJson(parametroAux);

        return testService(mensajeAEncriptar, Servicios.generarDocumento, leerArchivo("C:\\Users\\joans\\Desktop\\Tesis_Joan (1).pdf"));
    }

    public static boolean validarUsuario(String username, String password, String ldap) {
        String base = "ou=sade,dc=gob,dc=ar";
        String dn = "cn=" + username.toUpperCase() + "," + base;
        String ldapURL = ldap;
        boolean res = false;
        if (ldap.equals("")) {
            ldapURL = "ldap://ldap.gdetest.neuquen.gov.ar:389";
        }

        // Setup environment for authenticating
        Hashtable<String, String> environment
                = new Hashtable<String, String>();
        environment.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, ldapURL);
        environment.put(Context.SECURITY_AUTHENTICATION, "simple");
        environment.put(Context.SECURITY_PRINCIPAL, dn);
        environment.put(Context.SECURITY_CREDENTIALS, password);

        try {
            DirContext authContext
                    = new InitialDirContext(environment);
            res = true;
            System.out.println("okey");
            // user is authenticated
        } catch (AuthenticationException ex) {
            System.out.println("errror");
            // Authentication failed
        } catch (NamingException ex) {
            System.out.println("errror");

        }
        return res;

    }

    private static byte[] leerArchivo(String path) throws FileNotFoundException, IOException {
        byte[] array = Files.readAllBytes(Paths.get(path));

        String s = new String(array);
        // System.out.println(s);
        //      return s;
        return array;

    }

    private static String leerArchivoAux(String path) throws FileNotFoundException, IOException {
        byte[] array = Files.readAllBytes(Paths.get(path));
        String s = new String(array);
        byte[] prueba = Base64.getDecoder().decode(array);
        System.out.println(new String(prueba));

        //  System.out.println(s);
        return s;
        //      return array;

    }

}
